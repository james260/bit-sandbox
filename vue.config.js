// const EsmWebpackPlugin = require('@purtuga/esm-webpack-plugin');

module.exports = {
  lintOnSave: false,
  css: {
    extract: false,
  },
  chainWebpack: config => {
    // https://vue-svg-loader.js.org/faq.html#how-to-use-both-inline-and-external-svgs
    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();

    svgRule
      .oneOf('inline')
      .resourceQuery(/inline/)
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
      .options({
        svgo: {
          plugins: [
            { cleanupIDs: true },
            {
              prefixIds: {
                prefixIds: true,
                prefixClassNames: false,
              },
            },
            { removeXMLNS: true },
            { convertStyleToAttrs: true },
          ],
        },
      })
      .end()
      .end()
      .oneOf('external')
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]',
      });
  },
};
